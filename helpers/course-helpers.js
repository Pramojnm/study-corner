var db=require('../config/connection')
var collection=require('../config/collections')
const objectId=require('mongodb').ObjectID
const { ObjectID } = require('bson')
const { response } = require('express')

module.exports={

    addCourse:(course,callback)=>{
        db.get().collection('course').insertOne(course).then((data)=>{
            callback(data.insertedId)
        })
    },
    getAllCourse:()=>{
        return new Promise(async(resolve,reject)=>{
            let courses=await db.get().collection(collection.COURSE_COLLECTION).find().toArray()
            resolve(courses)
        })
    },
    deleteCourse:(courseId)=>{
        return new Promise((resolve,reject)=>{
            db.get().collection(collection.COURSE_COLLECTION).deleteOne({_id:objectId(courseId)}).then((response)=>{
                resolve(response)
            })
        })
    },
    getCourseDetails:(courseId)=>{
        return new Promise((resolve,reject)=>{
            db.get().collection(collection.COURSE_COLLECTION).findOne({_id:objectId(courseId)}).then((course)=>{
                resolve(course)
            })
        })
    },
    updateCourse:(courseId,courseDetails)=>{
        return new Promise((resolve,reject)=>{
            db.get().collection(collection.COURSE_COLLECTION)
            .updateOne({_id:objectId(courseId)},{
                $set:{
                    Name:courseDetails.Name,
                    Description:courseDetails.Description,
                    Price:courseDetails.Price,
                    Category:courseDetails.Category,
                    Trainer:courseDetails.Trainer,
                    Seats:courseDetails.Seats,
                    LongDescription:courseDetails.LongDescription
                }
            }).then((response)=>{
                resolve()
            })

            })
        },

        addTopCourse:(top_course,callback)=>{
            db.get().collection('top_course').insertOne(top_course).then((data)=>{
                callback(data.insertedId)
            })
        },
        getTopCourse:()=>{
            return new Promise(async(resolve,reject)=>{
                let top_course=await db.get().collection(collection.TOPCOURSE_COLLECTION).find().toArray()
                resolve(top_course)
           })
        },
        deleteTopCourse:(top_courseId)=>{
            return new Promise((resolve,reject)=>{
                db.get().collection(collection.TOPCOURSE_COLLECTION).deleteOne({_id:objectId(top_courseId)}).then((response)=>{
                    resolve(response)
                })
            })
        },
        addProject:(project,callback)=>{
            db.get().collection('project').insertOne(project).then((data)=>{
                callback(data.insertedId)
            })
        },
        getProject:()=>{
            return new Promise(async(resolve,reject)=>{
                let project=await db.get().collection(collection.PROJECT_COLLECTION).find().toArray()
                resolve(project)
            })
        },
        
        getProjectDetails:(projectId)=>{
            return new Promise((resolve,reject)=>{
                db.get().collection(collection.PROJECT_COLLECTION).findOne({_id:objectId(projectId)}).then((project)=>{
                    resolve(project)
                })
            })
        },
        updateProject:(projectId,projectDetails)=>{
            return new Promise((resolve,reject)=>{
                db.get().collection(collection.PROJECT_COLLECTION)
                .updateOne({_id:objectId(projectId)},{
                    $set:{
                        Name:projectDetails.Name,
                        Description:projectDetails.Description,
                        Price:projectDetails.Price,
                        Category:projectDetails.Category,
                        Trainer:projectDetails.Trainer,
                        Seats:projectDetails.Seats,
                        LongDescription:projectDetails.LongDescription
                    }
                }).then((response)=>{
                    resolve()
                })
    
                })
            },
            deleteProject:(projectId)=>{
                return new Promise((resolve,reject)=>{
                    db.get().collection(collection.PROJECT_COLLECTION).deleteOne({_id:objectId(projectId)}).then((response)=>{
                        resolve(response)
                    })
                })
            },

            addTrendingProject:(trending_project,callback)=>{
                db.get().collection('trending_project').insertOne(trending_project).then((data)=>{
                    callback(data.insertedId)
                })
            },

            getTrendingProject:()=>{
                return new Promise(async(resolve,reject)=>{
                    let trending_project=await db.get().collection(collection.TRENDING_PROJECT_COLLECTION).find().toArray()
                    resolve(trending_project)
               })
            },

            deleteTrendingProject:(trending_projectId)=>{
                return new Promise((resolve,reject)=>{
                    db.get().collection(collection.TRENDING_PROJECT_COLLECTION).deleteOne({_id:objectId(trending_projectId)}).then((response)=>{
                        resolve(response)
                    })
                })
            }

}