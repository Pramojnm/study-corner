const { response } = require('express');
var express = require('express');
const courseHelpers = require('../helpers/course-helpers');
var router = express.Router();

/* GET users listing. */


  
router.get('/',(req,res)=>{
  courseHelpers.getAllCourse().then((courses)=>{
    courseHelpers.getTopCourse().then((top_course)=>{
      courseHelpers.getProject().then((project)=>{
        courseHelpers.getTrendingProject().then((trending_project)=>{
          res.render('admin/courses',{admin:true,project,top_course,courses,trending_project})
        })

       
      })



})
  
})
  })


router.get('/add-top-courses',(req,res)=>{
  res.render('admin/add-top-course',{admin:true})
})

router.get('/add-course',function(req,res){
    res.render('admin/add-course',{admin:true})
    
})

router.post('/add-course',((req,res)=>{
  courseHelpers.addCourse(req.body,(id)=>{
    let image=req.files.Image
    console.log(id);
    image.mv('./public/course-images/'+id+'.jpeg',(err,done)=>{
      if(!err){
        res.render("admin/add-course",{admin:true})
      }else{
        console.log(err);
      }
    })
  })
}))

router.get('/delete-course/:id',(req,res)=>{
  let courseId=req.params.id
  console.log(courseId);
  courseHelpers.deleteCourse(courseId).then((response)=>{
    res.redirect('/admin')
  })
})
router.get('/edit-course/:id',async(req,res)=>{
  let course=await courseHelpers.getCourseDetails(req.params.id)
  console.log(course);
  res.render('admin/edit-course',{admin:true,course})
})
router.post('/edit-course/:id',(req,res)=>{
  let id=req.params.id
    courseHelpers.updateCourse(req.params.id,req.body).then(()=>{
      res.redirect('/admin')
      if(req.files.Image){
        let image=req.files.Image
        image.mv('./public/course-images/'+id+'.jpeg')
        
      }
    })
  })

router.post('/add-top-course',(req,res)=>{
  courseHelpers.addTopCourse(req.body,(id)=>{
    let image=req.files.Image
    console.log(id);
    image.mv('./public/top-course-images/'+id+'.jpeg',(err,done)=>{
      if(!err){
        res.render('admin/add-top-course',{admin:true})
      }else{
        console.log(err);
      }
    })
  })
})


router.get('/delete-top-course/:id',(req,res)=>{
  let top_courseId=req.params.id
  console.log(top_courseId);
  courseHelpers.deleteTopCourse(top_courseId).then((response)=>{
    res.redirect('/admin')
  })
})

router.get('/add-project',(req,res)=>{
  res.render('admin/add-project')
})

router.post('/add-project',(req,res)=>{
  courseHelpers.addProject(req.body,(id)=>{
    let image=req.files.Image
    image.mv('./public/project-images/'+id+'.jpeg',(err,done)=>{
      if(!err){
        res.render('admin/add-project',{admin:true})
      }else{
        console.log(err);
      }
    })
  
  })
  
})


router.get('/delete-project/:id',(req,res)=>{
  let projectId=req.params.id
  console.log(projectId);
  courseHelpers.deleteProject(projectId).then((response)=>{
    res.redirect('/admin')
  })
})
router.get('/edit-project/:id',async(req,res)=>{
  let projects=await courseHelpers.getProjectDetails(req.params.id)
  console.log(projects);
  res.render('admin/edit-project',{admin:true,projects})
})

router.post('/edit-project/:id',(req,res)=>{
  let id=req.params.id
    courseHelpers.updateProject(req.params.id,req.body).then(()=>{
      res.redirect('/admin')
      if(req.files.Image){
        let image=req.files.Image
        image.mv('./public/project-images/'+id+'.jpeg')
        
      }
    })
})

router.get('/add-trending-project',(req,res)=>{
  res.render('admin/add-trending-project',{admin:true})
})

router.post('/add-trending-project',(req,res)=>{
  courseHelpers.addTrendingProject(req.body,(id)=>{
    let image=req.files.Image
    console.log(id);
    image.mv('./public/trending-project-images/'+id+'.jpeg',(err,done)=>{
      if(!err){
        res.render('admin/add-trending-project',{admin:true})
      }else{
        console.log(err);
      }
    })
  })
})

router.get('/delete-trending-project/:id',(req,res)=>{
  let trending_projectId=req.params.id
  console.log(trending_projectId);
  courseHelpers.deleteTrendingProject(trending_projectId).then((response)=>{
    res.redirect('/admin')
  })
})

module.exports = router;
