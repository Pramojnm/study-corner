var express = require('express');
var router = express.Router();
const res = require('express/lib/response');
const courseHelpers = require('../helpers/course-helpers');
const userHelpers=require('../helpers/user-helpers')

/* GET home page. */
router.get('/', function(req, res, next) {
  let user=req.session.user
  console.log( user)
  courseHelpers.getTopCourse().then((top_course)=>{
    courseHelpers.getTrendingProject().then((trending_project)=>{
      res.render('user/home',{user,top_course,trending_project});
    })
    
    
  
})
  
});

router.get('/login',(req,res)=>{
  if(req.session.loggedIn){
    res.redirect('/')
  }else{

  res.render('user/login',{"loginErr":req.session.LoginErr})
  req.session.LoginErr=false
}
})
  

router.get('/signup',(req,res)=>{
  res.render('user/signup')

})

router.post('/signup',(req,res)=>{
  userHelpers.doSignup(req.body).then((response)=>{
    console.log(response)
    req.session.loggedIn=true
    req.session.user=response
    
    res.redirect('/')
  })

})
router.post('/login',(req,res)=>{
  userHelpers.doLogin(req.body).then((response)=>{
    if (response.status){
      req.session.loggedIn=true
      req.session.user=response.user
      
      
      res.redirect('/')
    }else{
      req.session.LoginErr="Invalid Username or Password"
      res.redirect('/login')
    }
  })
})

router.get('/course',(req,res)=>{
  let user=req.session.user
  courseHelpers.getAllCourse().then((courses)=>{
    res.render('user/course',{courses,user})
  })
  
})

router.get('/courseDetails/:id',async(req,res)=>{
  let user=req.session.user
  let course=await courseHelpers.getCourseDetails(req.params.id)
  console.log(course);
  res.render('user/course-details',{course,user})
})

router.get('/project',(req,res)=>{
  let user=req.session.user
  courseHelpers.getProject().then((project)=>{
    res.render('user/project',{project,user})
  })
  
})

router.get('/projectDetails/:id',async(req,res)=>{
  let user=req.session.user
  let project=await courseHelpers.getProjectDetails(req.params.id)
  res.render('user/project-details',{project,user})
})
module.exports = router;
